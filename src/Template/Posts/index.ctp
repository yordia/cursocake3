<h1>mis postssssssss</h1>
<?= $this->Html->link('Agregar post',['action'=>'add'],['class'=>'btn btn-primary']); 
?>
<table class='table table-hover table-bordered'>
    <thead>
    <tr>
        <th>Título</th>
        <th>Fecha de creacíon</th>
        <th>Acciones</th>

    </tr>
    </thead>
    <tbody>
        
    </tbody>

<?php foreach ($posts as $post): ?>
    
    <tr>
        <td>
            <?=
                $this->Html->link($post->title,['action'=>'view',$post->slug]);
            ?>
        </td>
        <td>
            <?= $post->created->format(DATE_RFC850) ?>
        </td>
        <td>
            <?= $this->Html->link('Editar',['action'=>'edit',$post->slug])  ?>
            <?=
                $this->Form->postLink('Eliminar',['action'=>'delete',$post->slug],['confirm'=>'Estas seguro de eliminar el postz'.$post->title])
            ?>
        </td>
    </tr>
<?php endforeach; ?>

</table>
