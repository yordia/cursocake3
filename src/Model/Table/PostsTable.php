<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;

class PostsTable extends Table
{
    public function beforeSave($event,$entity,$options)
    {
        if($entity->isNew() && !$entity->slug){
            $slug=Text::slug($entity->title);

            $entity->slug=substr($slug,0,191);
            // $entity->slug=substr($slug,start:0,length:191);
            // lexico DIFERENTE 4
        }
    }
    public function validationDefault(Validator $validator ){
        $validator
            ->notEmpty('title','El titulo no puede estar vacio')
            ->add('title', [
                'length' => [
                    'rule' => ['minLength', 10],
                    'message' => 'Como minimo 10 caracteres',
                ]
            ])
            // ->minLength('title', 10)
            ->maxLength('title',255)
            ->notEmpty('body')
            ->minLength('body',10);
        return $validator;
    }
    public function initialize(array $config)
    {
        // parent::initialize($config);
        $this->addBehavior('Timestamp');
        // $this->addBehavior(name:'Timestamp');
        // lexico 5 DIFERENTE
        $this->belongsToMany('Categories');
    }

}