<?php
namespace App\Controller;

use App\Controller\AppController;

class PostsController extends AppController
{
    public function __construct($request=null,$response=null,$name=null,$eventManager=null,$components=null){
        parent::__construct($request,$response,$name,$eventManager,$components);

        //cambiando layout
        $this->viewBuilder()->setLayout('webtraining-zone');

    }
    public function index()
    {
        // ORM:OBJECT Relational Mapping
        $posts = $this->Posts->find();

        // segun los errores lo correji
        $this->set(compact('varname','posts'));
        // codigo del profe error #1
        // $this->set(compact(varname : 'posts'));
    
    }
    public function view($slug=null)
    {
        $post=$this->Posts->findBySlug($slug)->firstOrfail();
        $this->set('post',$post);
    }
    public function add(){
        $post=$this->Posts->newEntity();

        if($this->request->is('post')){ 
            //2  error lexico
            // if($this->request->is(type:post')){

            $post=$this->Posts->patchEntity($post,$this->request->getData());
            $post->user_id=1;
            if($this->Posts->save($post)){
                $this->Flash->success('Post guardado correctamente'); 
                //3 error lexico
                // (message: 'Post guardado correctamente');
                // ('message','Post guardado correctamente'); si lo envio asi solo muestra el 1er parametro

                return $this->redirect(['action'=> 'index']);
            }
        }
        // obteniendo lista de categorias
        $categories=$this->Posts->Categories->find('list');
        $this->set('categories',$categories);
        
        $this->set('post',$post);
    }
    public function edit($slug){
        $post=$this->Posts
            ->findBySlug($slug)
            ->contain('Categories') //cargando categorias 
            ->firstOrFail();

        if($this->request->is(['post','put'])){ 
            //diferente-6 lexico
            // if($this->request->is(type:post')){

            $post=$this->Posts->patchEntity($post,$this->request->getData());
            $post->user_id=1;
            if($this->Posts->save($post)){
                $this->Flash->success('Post guardado correctamente'); 
                //diferente-7 lexico 
                // (message: 'Post guardado correctamente');
                // ('message','Post guardado correctamente'); si lo envio asi solo muestra el 1er parametro

                return $this->redirect(['action'=> 'index']);
            }
            $this->Flash->error('No se ha podido guardar');
            // diferente-8 messagee:
        }
        // obteniendo lista de categorias
        $categories=$this->Posts->Categories->find('list');
        $this->set('categories',$categories);

        $this->set('post',$post);
    }
    public function delete($slug){
        $this->request->allowMethod(['post','delete']);

        $post=$this->Posts->findBySlug($slug)->firstOrFail();
        if($this->Posts->delete($post)){
            $this->Flash->Success('Post eliminado correctamente');
            
            return $this->redirect(['action'=>'index']);
        }
        $this->Flash->error('No se ha podido eliminar el post');
    }
}